# jquery.qrcode.js  自定义logo加边框处理

#### 介绍
依照jquery.qrcode.js框架实现：  
1、自定义logo
2、边框优化处理，原二维码无边框

#### 软件架构
软件架构说明


#### 安装教程

1.  引用  jquery.js 
2.  引用  jquery.qrcode.js  

#### 使用说明
   在 document.createElement("canvas")  创建后添加以下代码（源码已添加  若需自定义则修改相应数值）
                           //加边框
    					   d.lineWidth=40,
    					   d.strokeStyle="#fff"; 
    					   d.strokeRect(0, 0, c.width,c.height), 
    					   d.save(), 
    					   //添加logo
                           d.drawImage(e, (b.width - b.imgWidth+40) / 2, (b.height - b.imgHeight+40) / 2, b.imgWidth, b.imgHeight)
    					   d.save() 
 
